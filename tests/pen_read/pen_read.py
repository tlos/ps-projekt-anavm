
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from common.timer import timer
from common.csvwriter import save_result
from time import sleep
from common.fileUtils import createFile
from common.fileUtils import removeFile

skip = False

def compile():
	os.system("make")

def clean():
	os.system("make clean")

@timer(trials = 5)
def invoke(file_name):
	os.system("./pen_read %s" % (file_name))

def test():
	if skip:
		return

	file_name = "/media/tomek/PENDRIVE/test_read_file"
	for file_size in [500, 5000, 50000, 500000]:
		createFile(file_name, file_size)
		print("Invoking pen_read with %d KB size ->" % (file_size))
		last = invoke(file_name)
		save_result('../../results/pen_read.csv', file_size, last);
		removeFile(file_name)
		

compile()
test()
clean()
