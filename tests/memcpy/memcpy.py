
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from common.timer import timer
from common.csvwriter import save_result



def compile():
	os.system("make")

def clean():
	os.system("make clean")

@timer(trials = 5)
def invoke(bytes, times):
	os.system("./memcpy %d %d" % (bytes, times))

def test():
	rounds = 100
	for t in [500, 50000, 500000, 5000000]:
		print("Invoking memcpy for %d bytes and %d rounds->" % (t,rounds))
		last = invoke(t,rounds)
		save_result('../../results/memcpy.csv', t, last);




compile()
test()
clean()
