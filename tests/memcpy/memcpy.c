#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define RETURN_ERROR -19999
#define RETURN_SUCCESS 0

void generate_random_values(char *mem, long bytes){
	int i = 0;
	for( i=0; i<bytes; i++ )
	{
		mem[i] = (char) (rand() % 127);
	}
}


void copy(char *dst, char *src, long bytes){
	memcpy( dst, src, (size_t)bytes);
}

void mix_memory( char *mem, long bytes){
	int i;
	for( i=0; i<bytes; i++ )
	{
		mem[rand()%bytes] = mem[rand()%bytes];
	}
}


int memory_cpy_test(long bytes){
	char *src = (char*)malloc(bytes);
	char *dst = (char*)malloc(bytes);
	if( src == NULL || dst == NULL )
	{
		return RETURN_ERROR;
	}
	
	generate_random_values(src,bytes);
	copy(dst,src,bytes);
	mix_memory(dst,bytes);

	free(src);
	free(dst);
	return RETURN_SUCCESS;
}


void exit_ok(){
	printf("OK\n");
	exit(0);
}
void exit_err(char *msg){
	printf("FAIL\n%s\n",msg);
	exit(1);
}

void print_usage(char *prog_name){
	printf("Usage: %s bytes_to_operate repeat_times\n",prog_name);
}
/**
* argv format: ./prog_name bytes_to_operate repeat_times
*/
int main(int argc, char **argv){
	if( argc < 3 ){
		//print_usage(argv[0]);
		exit_err("Wrong usage!");
	}
	long bytes_to_operate = atol(argv[1]);
	long times = atol(argv[2]);
	if( bytes_to_operate <= 0 || times <=  0){
		exit_err("Wrong params");
	}

	long i = 0;
	for(i = 0; i < times; i ++) {
		if(memory_cpy_test(bytes_to_operate) == RETURN_ERROR){
			exit_err("Memory operations error");
		}
	}
	exit_ok();
}