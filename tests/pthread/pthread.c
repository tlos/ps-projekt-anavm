#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define RETURN_ERROR -19999
#define RETURN_SUCCESS 0

//#define __DEB

int thread_num;
int sleep_in_thread_fun = 0;

pthread_t thread_tids[1000];

void exit_ok(){
	printf("OK\n");
	exit(0);
}

void exit_err(char *msg){
	printf("FAIL\n%s\n",msg);
	exit(1);
}

void valid_input(int argc, char ** argv){
	if( argc < 2 ){
		exit_err("Wrong usage!");
	}

	thread_num = atoi(argv[1]);

	if( thread_num <= 0 ){
		exit_err("Wrong params");
	}	
}

void* thread_fun(void *args){
	#ifdef __DEB
	printf("[T] My TID: %d\n", pthread_self() );
	#endif

	sleep(sleep_in_thread_fun);
	pthread_exit(NULL);
}

void create_threads(){
	#ifdef __DEB
	printf("Creating %d threads\n", thread_num);
	#endif

	int thread_pid;
	int i;
	for (i=0; i < thread_num; i++){
		pthread_t thread;
	    thread_pid = pthread_create(&thread_tids[i], NULL, thread_fun, NULL);
	    if (thread_pid ){
	    	exit_err("wrror when clone()");
	    }
	  
	    #ifdef __DEB
	    printf("Done! Thread pid: %d\n", thread_pid);
	    #endif
	}
}

void join_threads(){
	#ifdef __DEB
	printf("Joining %d threads:\n", thread_num);
	#endif

	int i, rc;
	for (i =0 ; i<thread_num; i++){
		rc = pthread_join(thread_tids[i], NULL);
		if (rc){
			exit_err("join()");
		}


	}

}


/**
* argv format: ./clone threads_num 
*/

int main(int argc, char **argv){
	valid_input(argc, argv);
	create_threads();
	exit_ok();
}


