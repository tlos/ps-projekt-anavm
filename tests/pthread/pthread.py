
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from common.timer import timer
from common.csvwriter import save_result
from time import sleep



def compile():
	os.system("make")

def clean():
	os.system("make clean")

@timer(trials = 5)
def invoke(proc_num):
	os.system("./pthread %d" % (proc_num))

def test():

	for n in [10, 100, 200, 800]:
		print("Invoking clone with %d threads_num->" % (n))
		last = invoke(n)
		save_result('../../results/pthread.csv', n, last);
		sleep(3)




compile()
test()
clean()
