#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>


#define RETURN_ERROR -19999
#define RETURN_SUCCESS 0

//#define __DEB



#define BUFFER_SIZE 1024


void exit_ok(){
	printf("DONE\n");
	exit(0);
}

void exit_err(char *msg){
	printf("FAIL\n%s\n",msg);
	exit(1);
}

/**
* argv format: ./write file_name file_size
*/

int main(int argc, char **argv){
	if( argc < 3 ){
		exit_err("Wrong usage!");
	}

	int KB_to_write = atoi(argv[2]);
	void *buf = malloc(BUFFER_SIZE);
	memset(buf, 55, BUFFER_SIZE);

	int fd = open(argv[1], O_WRONLY | O_CREAT, S_IRWXU | S_IRWXG);
	if (fd == -1){
		exit_err("error opening file");
	}

	int write_bytes;
	int i;
	for (i=0; i < KB_to_write; i++){
		write_bytes = write (fd, &buf, BUFFER_SIZE);
		if (write_bytes == -1){
			close(fd);
			free(buf);
			exit_err("write error");
		}
	}

	close(fd);
	free(buf);
	exit_ok();
}


