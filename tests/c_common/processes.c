#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define RETURN_ERROR -19999
#define RETURN_SUCCESS 0

pid_t  (*forking_fun_pointer)(void) = NULL;
char  forking_fun_name[10] = {0};

int pid_table[1000];
int proc_num;
int sleep_in_child_fun = 1;

void exit_ok(){
	printf("DONE\n");
	exit(0);
}

void exit_err(char *msg){
	printf("FAIL\n%s\n",msg);
	exit(1);
}

void child_fun(pid_t pid){
	//printf("[Child] %d start\n", pid);
	if (sleep_in_child_fun)
		sleep(2);
	//printf("[Child] %d end\n", pid);
	exit(0);
}

void print_pid_table(){
	int i=0;
	//printf("======== created pid table:\n");
	for (i=0; i < proc_num; i++){
		printf("\t%d\n", pid_table[i]);
	}
}

void wait_on_children(){
	//printf("waiting on children\n");

	int i;
	int status;
	pid_t pid;
	for (i=0; i < proc_num; i++){
		pid = waitpid(pid_table[i], &status, 0 );
		if (!WIFEXITED(status)){
			printf("FAIL proc %d did not exited\n", pid_table[i]);
		}
	}

	//printf("all child finished\n");
}

int test_forking(){
	//printf("executing test_forking, fun: %s, proc_num: %d\n", forking_fun_name, proc_num);

	int i;
	pid_t pid;
	for (i=0; i < proc_num; i++){
		pid = forking_fun_pointer();

		if (pid == -1){
			exit_err("forking error");
		}else if (pid == 0){
			child_fun(getpid());
		}
		pid_table[i] = pid;
	}
//	printf("Executed %d proc, waiting on finish\n", proc_num);
//	print_pid_table();
	wait_on_children();
//	sleep(2);
	return RETURN_SUCCESS;
}


void valid_input(int argc, char ** argv){
	if( argc < 2 ){
		exit_err("Wrong usage!");
	}

	proc_num = atoi(argv[1]);

	if( proc_num <= 0 ){
		exit_err("Wrong params");
	}	
}


void run_test_fork(int argc, char ** argv){
	valid_input(argc, argv);

	strcpy(forking_fun_name, "fork()");
	forking_fun_pointer = fork;
	sleep_in_child_fun = 0;

	if (RETURN_SUCCESS == test_forking()){
		exit_ok();
	}else{
		exit_err("RETURN_ERROR");
	}


}



void run_test_vfork(int argc, char ** argv){
	valid_input(argc, argv);

	strcpy(forking_fun_name, "vfork()");
	forking_fun_pointer = vfork;
	sleep_in_child_fun = 0;

	if (RETURN_SUCCESS == test_forking()){
		exit_ok();
	}else{
		exit_err("RETURN_ERROR");
	}
}

