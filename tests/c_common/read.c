#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#define RETURN_ERROR -19999
#define RETURN_SUCCESS 0

//#define __DEB

#define BUFFER_SIZE 2048
int buffer [BUFFER_SIZE];


void exit_ok(){
	printf("DONE\n");
	exit(0);
}

void exit_err(char *msg){
	printf("FAIL\n%s\n",msg);
	exit(1);
}

/**
* argv format: ./read file_name buff_size
*/

int main(int argc, char **argv){
	if( argc < 2 ){
		exit_err("Wrong usage!");
	}
	
	int fd = open(argv[1], O_RDONLY);
	if (fd == -1){
		exit_err("error opening file");
	}

	int read_bytes;
	while((read_bytes = read (fd, &buffer, BUFFER_SIZE)) > 0){
		if (read_bytes == -1){
			close(fd);
			exit_err("read error");
		}
	}

	close(fd);
	exit_ok();
}


