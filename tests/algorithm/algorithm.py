
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from common.timer import timer
from common.csvwriter import save_result



def compile():
	os.system("make")

def clean():
	os.system("make clean")

@timer(trials = 5)
def invoke(intervals_number, times):
	os.system("./algorithm %d %d" % (intervals_number, times))

def test():
	rounds = 100
	for t in [100000, 1000000, 10000000, 100000000]:
		print("Invoking algorithm for %d intervals_number and %d rounds->" % (t,rounds))
		last = invoke(t,rounds)
		save_result('../../results/algorithm.csv', t, last);




compile()
test()
clean()
