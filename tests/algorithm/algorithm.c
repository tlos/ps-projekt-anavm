#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define RETURN_ERROR -19999
#define RETURN_SUCCESS 0



#define RANGE_END 100.0
#define ERROR_RANGE 20.0

inline double integrate( double (*fun)(double x), double a, double b, const int intervals)
{
	if( intervals % 2 != 0 ){
		return -1.0;
	}
	int k = intervals/2;
	double h = ( b - a ) / ((double)intervals);
	double sum_1 = 0.0;
	int i = 0;
	double x = 0.0;
	for( i = 1; i <= k; ++i ){
		x = a + (2*i-1)*h; 
	 	sum_1 += fun( x );
	}
	double sum_2 = 0.0;
	x = a;
	for( i = 1; i < k; ++i ){
		x = a + (2*i-1)*h;
		sum_2 += fun( x );
	}
	return (h/3)*(fun(a) + 4*sum_1 + 2*sum_2 + fun(b));
}



void exit_ok(){
	printf("OK\n");
	exit(0);
}
void exit_err(const char *msg){
	printf("FAIL\n%s\n",msg);
	exit(1);
}

void print_usage(const char *prog_name){
	printf("Usage: %s intervals_number times\n",prog_name);
}

inline double function_to_integrate(double x){
	return 0.5*x;
}


/**
* argv format: ./prog_name intervals_number times
*/
int main(int argc, char **argv){
	if( argc < 3 ){
		//print_usage(argv[0]);
		exit_err("Wrong usage!");
	}
	long intervals_number = atol(argv[1]);
	long times = atol(argv[2]);
	if( times <= 0 || intervals_number <=  0){
		exit_err("Wrong params");
	}

	long i;
	for( i=0; i < times; i++ )
	{
		double result = integrate( function_to_integrate, 0.0, RANGE_END , (int)intervals_number );
		
		double exact_result = (RANGE_END*function_to_integrate(RANGE_END)) / 2.0;

		//printf("result=%.3f exact_result=%.3f\n",result,exact_result);
		
		if( result < exact_result - ERROR_RANGE || result > exact_result + ERROR_RANGE ){
			exit_err("Error with result");
		}
	}
	exit_ok();
}