'''csvwriter'''

separator = ';'
numeric_our_separator = ','


def get_computer_name():
	'''if exists variable HOSTNAME get from there else invokes 'hostname' subprocess and gets output'''
	name = ''
	import os
	try:
		name = os.environ['HOSTNAME']
	except KeyError:
		import subprocess
		p = subprocess.Popen(['hostname'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out,err = p.communicate()
		name = out.strip() if len(err) == 0 else 'unknown'
	return name



def save_result(file_path, parameter, result_time, append = True):
	hostname = get_computer_name()
	with open( file_path, 'a' if append else 'w') as f:
		f.write( hostname + separator + str(parameter) + separator+ str(result_time).replace('.',numeric_our_separator) + "\n")