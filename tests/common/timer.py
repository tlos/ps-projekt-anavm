
import time

class timer:
	'''If trials != 1 then function
	is invoked trials times and we take avg of running time'''
	def __init__(self,trials):
		self.trials = trials
		self.elapsed = []
	

	def __call__(self, fun):
		self.fun = fun
		return self.timed



	def timed(self,*args):
		for i in xrange(0,self.trials):
			start = time.time()
			self.fun(*args)
			stop = time.time()
			self.elapsed.append(stop - start)
		to_return = reduce( lambda x,y: x+y , self.elapsed)/self.trials	
		self.elapsed = []
		return to_return
		