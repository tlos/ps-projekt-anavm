import random
import string
import os




def createFile(name, sizeInKB):
	f = open(name, 'wb')
		
	for kb in xrange(0, sizeInKB):
		#line = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in xrange(1024))		
		line = ''.join('a' for i in xrange (1024))
		f.write(line)

	f.close()

def removeFile(name):
	if (os.path.exists(name)):
		os.remove(name)
