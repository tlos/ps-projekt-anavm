
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from common.timer import timer
from common.csvwriter import save_result



def compile():
	os.system("make")

def clean():
	os.system("make clean")

@timer(trials = 5)
def invoke(proc_num):
	os.system("./vfork %d" % (proc_num))

def test():

	for n in [10, 100, 200, 800]:
		print("Invoking vfork with %d proc_num->" % (n))
		last = invoke(n)
		save_result('../../results/vfork.csv', n, last);
		from time import sleep




compile()
test()
clean()
