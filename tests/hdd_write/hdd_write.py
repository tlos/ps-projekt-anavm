
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from common.timer import timer
from common.csvwriter import save_result
from time import sleep
from common.fileUtils import createFile
from common.fileUtils import removeFile



def compile():
	os.system("make")

def clean():
	os.system("make clean")

@timer(trials = 5)
def invoke(file_name, file_size):
	os.system("./hdd_write %s %d" % (file_name, file_size))

def test():
	file_name = "test_write_file"
	#for file_size in [1]:
	for file_size in [500, 5000, 50000, 500000]:
		removeFile(file_name)
		print("Invoking hdd_write with %d KB size ->" % (file_size))
		last = invoke(file_name, file_size)
		save_result('../../results/hdd_write.csv', file_size, last);
		removeFile(file_name)
		

compile()
test()
clean()
