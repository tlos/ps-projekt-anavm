#!/usr/bin/env python
#-*-coding: utf-8 -*-
#
#	MAIN TESTS RUNNER
#
# 	@author Tomasz Łoś, Tomasz Dziok
# 	
#	11.10.2013

url = r'https://bitbucket.org/tlos/ps-projekt-anavm/get/master.tar.gz'


import logging as log	
log.basicConfig(level=log.INFO)

import os
work_dir = os.getcwd()

def download_code_from_repo():
	log.info("Downloading source code")
	os.system("mkdir install")
	os.system("wget %s" % url)
	os.system("tar xfz master.tar.gz -C install")
	os.chdir( "install/%s" % (os.listdir("install/")[0]) )
	print( " currnet dir=%s" % os.getcwd()) 
	os.system("cp -r tests config ../../")
	os.chdir(work_dir)
	os.system("rm install -r master.tar.gz")
	os.system("mkdir results")

import sys
if "win" in sys.platform:
	print("Bez jaj. Ten skrypt jest na linuxa!")
	exit()

from os.path import isdir
if not isdir("config") or not isdir("tests"):
	download_code_from_repo()





from config import config
python_cmd 	= config.PYTHON_INTERPRETER_RUN_CMD
tests 		= config.TESTS




def run_test( test ):
	os.chdir( tests[test] ) #change working dir to dir with test_case
	os.system( "%s %s.py" % (python_cmd, test) ) #invoke
	os.chdir( work_dir ) #change back working directory

def run_tests():
	log.info("Cleaning results dir");
	os.system("rm -rf results/*")
	os.system("rm -rf results")
	os.system("mkdir results")		
	log.info("Running tests->")
	for test in tests:
		log.info("\t###################\n")
		log.info("\tRunning test %s \n" % test)
		log.info("\t###################\n")
		run_test( test )
	from time import sleep
	sleep(5);
	log.info("Ran all tests")

def clean():
	log.info("Cleaning->")
	for test in tests:
		os.chdir( tests[test] ) #change working dir to dir with test_case
		os.system( "make clean" ) #invoke
		os.chdir( work_dir ) #change back working directory
	os.system("rm -fr results/*")



if len(sys.argv) > 1 and sys.argv[1] == 'clean':
	clean()
else:
	run_tests()
