# CONFIGURATION FOR ALL TESTS

PYTHON_INTERPRETER_RUN_CMD = "python"

TESTS = { 
'algorithm':'tests/algorithm',
'pthread':'tests/pthread',
'fork':'tests/fork',
'hdd_read':'tests/hdd_read',
'hdd_write':'tests/hdd_write',
'memcpy':'tests/memcpy', 
'pen_write':'tests/pen_write',
'pen_read':'tests/pen_read',
'vfork':'tests/vfork'
}
